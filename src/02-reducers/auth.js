import {ACTIONS} from '../01-actions/auth';

const getUserFromLocalStorage = () => {
	let tokenExpiration = localStorage.getItem("USER_ACCESS_TOKEN_EXPIRATION");
	if (!tokenExpiration || Number(tokenExpiration) < Date.now())
		return null;
	return {token: localStorage.getItem("USER_ACCESS_TOKEN")};
};

const initialState = {
	status: null,
	user: getUserFromLocalStorage(),
	authError: null,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case ACTIONS.LOGIN_PROGRESS:
		case ACTIONS.REGISTER_PROGRESS:
		case ACTIONS.REGISTER_SUCCESS:
			return {
				...state,
				status: action.type
			};
		case ACTIONS.LOGIN_SUCCESS:
			localStorage.setItem("USER_ACCESS_TOKEN", action.data.user.token);
			localStorage.setItem("USER_ACCESS_TOKEN_EXPIRATION", action.data.user.tokenExpiration);
			return {
				...state,
				status: action.type,
				user: {token: action.data.user.token}
			};
		case ACTIONS.LOGIN_FAILED:
		case ACTIONS.REGISTER_FAILED:
			return {
				...state,
				status: action.type,
				authError: action.data.error
			};
		case ACTIONS.LOGOUT:
			localStorage.removeItem("USER_ACCESS_TOKEN");
			localStorage.removeItem("USER_ACCESS_TOKEN_EXPIRATION");
			return {
				...state,
				status: action.type,
				user: null
			};
		default:
			return state;
	}
}