import React, {Component} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import {renderToStaticMarkup} from 'react-dom/server';
import {withLocalize} from 'react-localize-redux';

import './App.css';

import enTranslation from '../../06-assets/translations/en.json';
import viTranslation from '../../06-assets/translations/vi.json';

import HomePage from '../_pages/Home';
import AuthPage from '../_pages/Auth';
import AdminPage from '../_pages/Admin';
import ErrorPage from '../_pages/Error';


class App extends Component {
	constructor(props) {
		super(props);
		
		this.props.initialize({
			languages: [
				{code: "en"},
				{code: "vi"},
			],
			options: {renderToStaticMarkup}
		});
		this.props.addTranslationForLanguage(enTranslation, "en");
		this.props.addTranslationForLanguage(viTranslation, "vi");
	}
	
	render() {
		return (
			<div className="app">
				<Switch>
					<Route exact path="/" component={HomePage}/>
					<Route path="/auth" component={AuthPage}/>
					<Route path="/admin" component={AdminPage}/>
					<Route path="/error" component={ErrorPage}/>
					<Redirect to="/error/404"/>
				</Switch>
			</div>
		);
	}
}

export default withLocalize(App);
