import React, {Component} from 'react';
import {Switch, Route, Redirect, NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import {Button} from 'reactstrap';

import {logout} from '../../../01-actions/auth';
import './style.css';
import Dashboard from './Dashboard';
import Function1Page from './Function1';
import Function2Page from './Function2';

class AdminPage extends Component {
	constructor(props){
		super(props);
		this.handleLogout = this.handleLogout.bind(this);
	}
	
	handleLogout(){
		this.props.logout();
	}
	
	render() {
		return (
			<div className="pages-admin">
				{!this.props.user &&
				<Redirect to="/auth/login"/>
				}
				<div className="admin-nav-bar">
					<NavLink exact to="/admin">Dashboard</NavLink>
					<NavLink to="/admin/function1">Function 1</NavLink>
					<NavLink to="/admin/function2">Function 2</NavLink>
				</div>
				<div>
					<Button onClick={this.handleLogout}>Logout</Button>
				</div>
				<Switch>
					<Route exact path="/admin" component={Dashboard}/>
					<Route path="/admin/function1" component={Function1Page}/>
					<Route path="/admin/function2" component={Function2Page}/>
					<Redirect to="/error/404"/>
				</Switch>
			</div>
		);
	}
}


const mapStateToProps = (store) => {
	return {
		user: store.auth.user
	}
};


const mapDispatchToProps = (dispatch) =>{
	return {
		logout: ()=>{
			dispatch(logout());
		}
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminPage);