import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
	Card,
	CardBody,
	CardTitle,
	FormGroup,
	Input,
	Button,
} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import Alert from '../../../../03-components/Alert';
import {ACTIONS, login} from '../../../../01-actions/auth';

import './style.css';

class LoginPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			username: '',
			password: '',
			alertAck: false
		};
		this.handleValueChange = this.handleValueChange.bind(this);
		this.handleLogin = this.handleLogin.bind(this);
		this.alertAck = this.alertAck.bind(this);
	}
	
	handleValueChange(field) {
		return (e) => {
			this.setState({[field]: e.target.value});
		}
	}
	
	handleLogin() {
		this.setState({alertAck: false});
		this.props.login({username: this.state.username, password: this.state.password});
	}
	
	alertAck() {
		this.setState({alertAck: true});
	}
	
	render() {
		return (
			<div className="pages-auth-login">
				{this.props.authStatus === ACTIONS.LOGIN_SUCCESS && (
					<Redirect to="/admin"/>
				)}
				<Card>
					<CardBody>
						<CardTitle>Login form</CardTitle>
						<fieldset disabled={this.props.authStatus === ACTIONS.LOGIN_PROGRESS}>
							<FormGroup>
								<Input value={this.state.username} onChange={this.handleValueChange('username')}
								       type="text" placeholder="Username"/>
							</FormGroup>
							<FormGroup>
								<Input value={this.state.password} onChange={this.handleValueChange('password')}
								       type="password" placeholder="Password"/>
							</FormGroup>
							<div className="text-center">
								<Button
									className={this.props.authStatus === ACTIONS.LOGIN_PROGRESS ? "progress-button" : ""}
									onClick={this.handleLogin}
									disabled={!this.state.username || !this.state.password}
									color="primary">Login</Button>
							</div>
						</fieldset>
					</CardBody>
				</Card>
				{(this.props.authStatus === ACTIONS.LOGIN_FAILED) && !this.state.alertAck && (
					<Alert header="Error" content={this.props.authError.code} okTitle="OK" onClose={this.alertAck}/>
				)}
			</div>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		authStatus: store.auth.status,
		authError: store.auth.authError,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		login: (data) => {
			dispatch(login(data));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);