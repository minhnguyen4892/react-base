import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {
	Card,
	CardBody,
	CardTitle,
	FormGroup,
	Input,
	Button,
} from 'reactstrap';
import Alert from '../../../../03-components/Alert';
import {ACTIONS, register} from '../../../../01-actions/auth';

import './style.css';

class RegisterPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			username: '',
			password: '',
			repeatPassword: '',
			alertAck: false
		};
		this.handleValueChange = this.handleValueChange.bind(this);
		this.handleRegister = this.handleRegister.bind(this);
		this.alertAck = this.alertAck.bind(this);
	}
	
	handleValueChange(field) {
		return (e) => {
			this.setState({[field]: e.target.value});
		}
	}
	
	handleRegister() {
		this.setState({alertAck: false});
		this.props.register({username: this.state.username, password: this.state.password});
	}
	
	alertAck() {
		this.setState({alertAck: true});
	}
	
	render() {
		return (
			<div className="pages-auth-register">
				{this.props.authStatus === ACTIONS.REGISTER_SUCCESS && (
					<Redirect to="/auth/login"/>
				)}
				<Card>
					<CardBody>
						<CardTitle>Register form</CardTitle>
						<fieldset disabled={this.props.authStatus === ACTIONS.REGISTER_PROGRESS}>
							<FormGroup>
								<Input value={this.state.username} onChange={this.handleValueChange('username')}
								       type="text" placeholder="Username"/>
							</FormGroup>
							<FormGroup>
								<Input value={this.state.password} onChange={this.handleValueChange('password')}
								       type="password" placeholder="Password"/>
							</FormGroup>
							<FormGroup>
								<Input value={this.state.repeatPassword}
								       onChange={this.handleValueChange('repeatPassword')}
								       type="password" placeholder="Repeat password"/>
							</FormGroup>
							<div className="text-center">
								<Button
									className={this.props.authStatus === ACTIONS.REGISTER_PROGRESS ? "progress-button" : ""}
									onClick={this.handleRegister}
									disabled={!this.state.username || !this.state.password || (this.state.password !== this.state.repeatPassword)}
									color="primary">Register</Button>
							</div>
						</fieldset>
					</CardBody>
				</Card>
				{(this.props.authStatus === ACTIONS.REGISTER_FAILED) && !this.state.alertAck && (
					<Alert header="Error" content={this.props.authError.code} okTitle="OK" onClose={this.alertAck}/>
				)}
			</div>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		authStatus: store.auth.status,
		authError: store.auth.authError,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		register: (data) => {
			dispatch(register(data));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);