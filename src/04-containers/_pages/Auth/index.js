import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import RegisterPage from "./Register";
import LoginPage from "./Login";

import './style.css';

export default class AuthPage extends Component {
	render() {
		return (
			<div className="pages-auth">
				<Switch>
					<Route path="/auth/login" component={LoginPage}/>
					<Route path="/auth/register" component={RegisterPage}/>
					<Redirect to="/error/404"/>
				</Switch>
			</div>
		);
	}
}